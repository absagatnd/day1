﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class StudentController : Controller
    {
        public List<Student> Students { get; set; }

        public StudentController()
        {
            Students = new List<Student>()
            {
                new Student() {Id = 1, FirstName = "First1", LastName = "Last"},
                new Student() {Id = 2, FirstName = "Third1", LastName = "Fourth"}
            };
        }

        // localhost/Student/Index
        public ActionResult Index()
        {
            return RedirectToAction("List2");
        }
        
        public ActionResult List2()
        {
            return View(Students);
        }

        public ActionResult Edit(int id)
        {
            var student = Students.FirstOrDefault(x => x.Id == id);
            return View(student);
        }
    }
}